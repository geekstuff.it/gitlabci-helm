# Gitlab CI library for Helm projects

Simple repo to gather gitlab-ci parts and variants related to Helm projects.

## How to use

- Copy the following yaml block in your `.gitlab-ci.yml`.
  - You can replace publish-gitlab with another publish-something,
    if it was made. Otherwise you can propose it.
- Replace the url part `main` with a specific tag. Example `v0.1`.
  - Find out and use the latest tag for this repository. [link](https://gitlab.com/geekstuff.it/libs/gitlab-ci/helm/-/tags)

```yaml
include:
  - remote: 'https://gitlab.com/geekstuff.it/libs/gitlab-ci/helm/raw/main/common.yml'
  - remote: 'https://gitlab.com/geekstuff.it/libs/gitlab-ci/helm/raw/main/publish-gitlab.yml'
```

## How it acts

It includes the Branch-Pipelines workflow.

The CI jobs are:

- A lint job
- A package job, depending on lint, and generating an artifact.
- A publish job, depending on package and its artifact, and
  publishing somewhere.

The Package and Publish job will only run on tags.

Those git tags will be used as helm versions. So they must be a valid semver
version as Helm expects. This means the tags you create must be numbers and dots. No `v` prefix.
